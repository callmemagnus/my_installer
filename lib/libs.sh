DIR_INSTALLER=$HOME/.installer
DIR_VENDOR=$DIR_INSTALLER/vendor
DIR_MODULES=modules


TIMESTAMP=$(date "+%Y%m%dT%H%M%S")
CURRENT_MODULE=""

DIR_LIBS=$(cd `dirname $BASH_SOURCE` && pwd)

: ${DIR_CUSTOM_MODULES:=$DIR_LIBS/custom}


source $DIR_LIBS/utils.sh
source $DIR_LIBS/sudo.sh
source $DIR_LIBS/logging.sh
source $DIR_LIBS/packages_apt.sh
source $DIR_LIBS/fs.sh
source $DIR_LIBS/service.sh


function go () {
	# preparing
	mkdir -p $DIR_INSTALLER/

	local temp_modules_dir=$(mktemp -d)
	local modules_dir=$DIR_LIBS/../$DIR_MODULES

	# copy modules to temporary directory
	cp -r $modules_dir $temp_modules_dir
	# copy custom modules to temporary directory
	if [[ -d $DIR_CUSTOM_MODULES ]]; then
		cp -r $DIR_CUSTOM_MODULES/* $temp_modules_dir/$DIR_MODULES
		if [[ $? != 0 ]]; then
			echo "custom modules should not be named like base modules"
			rm -rf $temp_modules_dir
			exit
		fi
	fi

	echo "We'll may need sudo rights, please enter your password below"
	sudo echo "Thanks... let's continue!"

	for i in ${modules[@]}; do

		if [[ -x $temp_modules_dir/$DIR_MODULES/$i ]]; then
			CURRENT_MODULE=$i
			_separator $i
			local RETURN_DIR=$(pwd)
			cd $temp_modules_dir/$DIR_MODULES/$i
			source $i.sh
			cd $RETURN_DIR
		fi

	done

	rm -rf $temp_modules_dir
}
