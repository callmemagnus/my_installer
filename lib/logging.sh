
function _separator() {
	echo -e "\e[1m$1\e[21m"
}

function _title() {
	echo -e "$1"
}

function _log () {
	MSG="- $CURRENT_MODULE - $2"

	case $1 in
		debug )
			[[ $DEBUG ]] && echo "[d] $MSG"
			;;
		error )
			echo "[e] $MSG"
			;;
		info )
			echo "[i] $MSG"
			;;
	esac
}
