pm_packages=()

function __update_installed_packages () {
	pm_packages=$(dpkg --get-selections | grep "[^e]install" | cut -f 1 | sed 's/.*/[&]/')
}

function __is_installed () {
	_contains_element "[$1]" $pm_packages
	if [[ $? == 1 ]]; then
	 	_contains_element "[$1:$(dpkg --print-architecture)]" $pm_packages || return 1
	fi
	return 0
}

function _install () {
	__is_installed "$1"
	if [[ $? == 1 ]];
		then
		_log info "installing $1"
		sudo apt-get -q5 -y install $1
		__update_installed_packages
	fi
}

function _update () {
	sudo apt-get -q9 update
}

function _add_ppa () {
	find /etc/apt/ -name \*.list | xargs cat | grep  ^[[:space:]]*deb | grep `echo "$1" | cut -d':' -f2` > /dev/null
	if [[ $? -eq 1 ]];
		then
		_log info "adding ppa $1"
		sudo add-apt-repository $1
	fi
}

function _add_custom_repo () {
	# $1 is name of repo
	# $2 is repo line
	# $3 is key address
	if [ "$(find /etc/apt/sources.list.d/ -name $1.list)" == "" ];
		then
		log info "add custom repo for $1"
		sudo sh -c "echo '$2' > /etc/apt/sources.list.d/$1.list"
		wget -O - $3 | sudo apt-key add -
	fi
}

__update_installed_packages