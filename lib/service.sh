function _restart () {
	sudo service $1 restart
}

function _stop () {
	sudo service $1 stop
}

function _start () {
	sudo service $1 start
}