DIR_BACKUP=$DIR_INSTALLER/backups
HAS_BACKUP=0

function __fs_wrap_up () {
	CURRENT_MODULE="fs"
	if [[ $HAS_BACKUP == 1 ]]; then
		_log info ""
		_log info "Existing files have been backed up:"
		_log info $(find $DIR_BACKUP -name $TIMESTAMP*)
	fi
}

trap __fs_wrap_up EXIT

function __backup_and_delete () {
	mkdir -p $DIR_BACKUP
	_log info "\"$1\" already exists, backing up"

	if [[ -L $1 ]]; then
		# is a link delete it
		_log debug "\"$1\" is a link, deleting"
		rm $1
	else
		HAS_BACKUP=1
		mv $1 $DIR_BACKUP/$TIMESTAMP-$(basename $1)
	fi
}

function __backup_and_delete_with_sudo () {
	mkdir -p $DIR_BACKUP
	_log info "\"$1\" already exists, backing up"

	if [[ -L $1 ]]; then
		# is a link delete it
		_log debug "\"$1\" is a link, deleting"
		sudo rm $1
	else
		HAS_BACKUP=1
		sudo mv $1 $DIR_BACKUP/$TIMESTAMP-$(basename $1)
		# we don't change owner nor access rights as it may be a security leak
	fi
}

# works also for links
function __is_same_file () {
	diff $1 $2 -q > /dev/null && return 0 || return 1
}

function _link () {
	mkdir -p $(dirname $2)
	_log debug "link from $2 to $1"

	if [[ -f $2 ]]; then
		__is_same_file $1 $2 && return
		__backup_and_delete $2
	fi

	_log info "Link from $1 to $2"
	ln -s $1 $2
}

function _git () {
	mkdir -p $DIR_VENDOR
	DIR=$(basename $1 .git)

	local _git_RETURN_DIR=$(pwd)

	if [[ -d $DIR_VENDOR/$DIR ]]; then
		cd $DIR_VENDOR/$DIR
		_log debug "fetching $1"
		git pull --rebase -q
	else
		cd $DIR_VENDOR
		_log debug "updateing $1"
		git clone $1 -q
	fi
	cd $_git_RETURN_DIR

}

function _download () {
	RESOURCE_NAME=$(basename $2)
	mkdir -p $DIR_INSTALLER/downloads
	if [[ ! -a $DIR_INSTALLER/downloads/$RESOURCE_NAME ]]; then
		wget $1 -O $DIR_INSTALLER/downloads/$RESOURCE_NAME
	fi
	cp $DIR_INSTALLER/downloads/$RESOURCE_NAME $2
}


function _add_file () {
	_log debug "Adding file $1 at $2"
	mkdir -p $(dirname $2)

	if [[ ! -f $1 ]]; then
		_log error "file $1 does not exist"
		return
	fi

	if [[ -f $2 ]]; then
		__is_same_file $(pwd)/$1 $2 && return
		__backup_and_delete $2
	fi
	cp $1 $2

	if [[ ! -z $3 ]]; then
		chmod $3 $2
	fi
}

function _add_file_with_sudo () {
	_log debug "adding $1 at $2 with sudo"

	if [[ ! -f $1 ]]; then
		_log error "file $1 does not exist"
		return
	fi

	if [[ -f $2 ]]; then
		__is_same_file $(pwd)/$1 $2 && return
		__backup_and_delete_with_sudo $2
	fi
	$SUDO mkdir -p $(dirname $2)
	$SUDO cp $1 $2

	if [[ ! -z $3 ]]; then
		$SUDO chmod $3 $2
	fi
}