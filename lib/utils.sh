
function _join_array {
	local IFS="$1"; shift; echo "$*";
}

function _contains_element () {
	local e
	for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
	return 1
}
