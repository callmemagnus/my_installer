# my\_installer - another bash installation

Based on modules, an installation script will be like:

    #!/bin/bash
    # modules to install
    modules=(
        dev
        java7
        )
    source lib/libs.sh
    go

## Add your custom modules to the existing ones

In order to mix base modules, the ones provided in the `modules` directory, with your custom modules, you can, in your script add the `DIR_CUSTOM_MODULES` to point to your custom module directory.

Image you have this directory structure:

    - $HOME
        - installer
            - my_installer
                - modules
                - lib
            - my_modules
                - my-module-1
                - my-module-2

modify your script to look like this

    #!/bin/bash
    
    DIR_CUSTOM_MODULES=$HOME/installer/my_modules
    
    # modules to install
    modules=(
        dev
        java7
        my-module-1
        my-module-2
        )
    source lib/libs.sh
    go

Make sure the name of your module does not already exist in the `modules` directory.

A module is a directory and a file having the same name:

    - modules
      - dev
      - daw
      - my-module
         - my-module.sh

That file (my-module.sh) is:

    # my-module.sh
    
    do\_something...

You can use all the function described below.

### File system functions (lib/fs.sh)

- \_link and \_add\_file 
- \_link \[original\_file] \[target]
- \_add\_file \[source] \[destination] \[octal\_permission]
- \_add\_file\_with\_sudo \[source] \[destination] \[octal\_permission]
- \_download \[url] \[local\_name]

### package related functions (lib/packages\_apt.sh)

- \_install \[package\_name]
- \_update
- \_add\_ppa \[ppa:name]
- \_add\_custom\_repo \[url to repo] \[url to key]

### service related functions (lib/service.sh)

- \_restart \[service]
- \_start \[service]
- \_stop \[service]

### logging related functions (lib/logging.sh)

- \_log info \[msg]
- \_log error \[msg]
- \_log debug \[msg]