# zsh

#_depends_on dev

_install zsh
_install zsh-doc

_git https://github.com/robbyrussell/oh-my-zsh.git

_add_file files/zsh_rc ~/.zshrc
