# ardour


DAW_PACKAGES=(
	ardour
	pulseaudio-module-jack
	jack-rack
	ecasound
	guitarix
)

for i in ${DAW_PACKAGES[@]}; do
	_install $i
done
