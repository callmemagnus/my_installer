# cntlm installer
#
# tricky :-)


function __ping () {
	ping -q -w 1 -c 1 $1 > /dev/nulls
}


function __has_network () {
	# get route
	local gateway=$(ip r |grep default |cut -d ' ' -f 3)

	if [[ $gateway == "" ]]; then
		echo error
		return
	fi

	__ping $gateway
}

function __has_internet () {
	local host=www.ubuntu.com

	if [[ __has_network ]];
		then
		wget -q -O /dev/null $host && echo ok || echo error
	else
		echo error
	fi
}

if [[ $(__has_network) != "ok" ]]; then
	_log error "cannot continue, there are no network connections"
	exit
fi

if [[ $(__has_internet) != "ok" ]]; then
	_log info "no access to internet, let's install cntlm"
	read -p"Proxy host (ex. proxy.mycompany.com) : " __proxy_host
	read -p"Proxy port [8080] : " __proxy_port
	read -p"Domain : " __cntlm_domain
	read -p"Username : " __cntlm_username
	read -p"Password : " -s __cntlm_password

	# default values
	: ${__proxy_port:=8080}

	http_proxy="http://$__cntlm_username:$__cntlm_password@$__proxy_host:$__proxy_port"

	# exception
	sudo http_proxy=$http_proxy apt-get install cntlm

	# update configuration file for cntlm
	_cntlm_dir_temp=$(mktemp -d)
	cp files/config $_cntlm_dir_temp


	sed -i "s/^\(Proxy\).*$/\1 $__proxy_host:$__proxy_port/g" $_cntlm_dir_temp/config
	sed -i "s/\(Username\).*$/\1 $__cntlm_username/g" $_cntlm_dir_temp/config
	sed -i "s/\(Password\).*$/\1 $__cntlm_password/g" $_cntlm_dir_temp/config
	sed -i "s/\(Pass\).*$/\1 $__cntlm_password/g" $_cntlm_dir_temp/config
	sed -i "s/\(Domain\).*$/\1 $__cntlm_domain/g" $_cntlm_dir_temp/config
	sed -i "s/\(Workstation\).*$/\1 $(hostname)/g" $_cntlm_dir_temp/config

	# cat $_cntlm_dir_temp/config
	sudo cp $_cntlm_dir_temp/config /etc/cntlm.conf
	sudo chown root:root /etc/cntlm.conf
	sudo chmod 600 /etc/cntlm.conf

	rm -rf $_cntlm_dir_temp/config

	# backup dist version
	if [[ ! -f /etc/cntlm.conf.dist ]]; then
		sudo mv /etc/cntlm.conf{,.dist}
	fi


	# add proxy for apt based tools
	echo "
Proxy::http { Proxy: \"http://localhost:3128\"; };
Proxy::https { Proxy: \"http://localhost:3128\"; };
	" | sudo tee /etc/apt/apt.conf.d/30proxy


	_restart cntlm
fi


