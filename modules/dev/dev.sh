# dev package collection

packages=(
	git
	tig
	meld
	vim
	git-doc
	git-gui
	git-cola
	less
	multitail
	terminator
	python-virtualenv
)

for i in ${packages[@]}; do
	_install $i
done

 sudo update-alternatives --set x-terminal-emulator /usr/bin/terminator